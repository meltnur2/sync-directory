<!DOCTYPE html>
<html class="" lang="en">
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<link href="https://assets.gitlab-static.net" rel="dns-prefetch">
<link crossorigin="" href="https://assets.gitlab-static.net" rel="preconnnect">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="object" property="og:type">
<meta content="GitLab" property="og:site_name">
<meta content="Value stream analytics · Analytics · User · Help" property="og:title">
<meta content="GitLab.com" property="og:description">
<meta content="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png" property="og:image">
<meta content="64" property="og:image:width">
<meta content="64" property="og:image:height">
<meta content="https://gitlab.com/help/user/analytics/value_stream_analytics.md" property="og:url">
<meta content="summary" property="twitter:card">
<meta content="Value stream analytics · Analytics · User · Help" property="twitter:title">
<meta content="GitLab.com" property="twitter:description">
<meta content="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png" property="twitter:image">

<title>Value stream analytics · Analytics · User · Help · GitLab</title>
<meta content="GitLab.com" name="description">
<link rel="shortcut icon" type="image/png" href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" id="favicon" data-original-href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" />
<link rel="stylesheet" media="all" href="https://assets.gitlab-static.net/assets/application-04ced2b1a5f2e65cf8a7d24dbe27b8332f7377f5e6f04b44df2147fb6a03b264.css" />
<link rel="stylesheet" media="print" href="https://assets.gitlab-static.net/assets/print-74c3df10dad473d66660c828e3aa54ca3bfeac6d8bb708643331403fe7211e60.css" />


<link rel="stylesheet" media="all" href="https://assets.gitlab-static.net/assets/highlight/themes/white-a20fa0d18cb98944b079c02ad5a6f46cb362f986ffd703fda24b3e8e2a4a8874.css" />
<script nonce="zhAEJVHVkI6oCvToBLFaDg==">
//<![CDATA[
window.gon={};gon.api_version="v4";gon.default_avatar_url="https://assets.gitlab-static.net/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png";gon.max_file_size=10;gon.asset_host="https://assets.gitlab-static.net";gon.webpack_public_path="https://assets.gitlab-static.net/assets/webpack/";gon.relative_url_root="";gon.shortcuts_path="/help/shortcuts";gon.user_color_scheme="white";gon.sentry_dsn="https://526a2f38a53d44e3a8e69bfa001d1e8b@sentry.gitlab.net/15";gon.sentry_environment=null;gon.gitlab_url="https://gitlab.com";gon.revision="df2daad408f";gon.gitlab_logo="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png";gon.sprite_icons="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg";gon.sprite_file_icons="https://gitlab.com/assets/file_icons-7262fc6897e02f1ceaf8de43dc33afa5e4f9a2067f4f68ef77dcc87946575e9e.svg";gon.emoji_sprites_css_path="https://assets.gitlab-static.net/assets/emoji_sprites-289eccffb1183c188b630297431be837765d9ff4aed6130cf738586fb307c170.css";gon.test_env=false;gon.disable_animations=null;gon.suggested_label_colors={"#0033CC":"UA blue","#428BCA":"Moderate blue","#44AD8E":"Lime green","#A8D695":"Feijoa","#5CB85C":"Slightly desaturated green","#69D100":"Bright green","#004E00":"Very dark lime green","#34495E":"Very dark desaturated blue","#7F8C8D":"Dark grayish cyan","#A295D6":"Slightly desaturated blue","#5843AD":"Dark moderate blue","#8E44AD":"Dark moderate violet","#FFECDB":"Very pale orange","#AD4363":"Dark moderate pink","#D10069":"Strong pink","#CC0033":"Strong red","#FF0000":"Pure red","#D9534F":"Soft red","#D1D100":"Strong yellow","#F0AD4E":"Soft orange","#AD8D43":"Dark moderate orange"};gon.first_day_of_week=0;gon.ee=true;gon.features={"snippetsVue":true,"monacoSnippets":true,"monacoBlobs":false,"monacoCi":false,"snippetsEditVue":false};
//]]>
</script>

<script src="https://assets.gitlab-static.net/assets/webpack/runtime.a3a21926.bundle.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/main.1bbc6d00.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/sentry.b689f188.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/pages.help.show.be6ac6b0.chunk.js" defer="defer"></script>

<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="dKr4eanGRl2GtXbMTVN0FbgDVkeWX6idBvPTTHAX6235kQhqnIfoN9xraa75q6IJYMZNNRA/9TK4/aCsd+z/pw==" />
<meta name="csp-nonce" content="zhAEJVHVkI6oCvToBLFaDg==" />
<meta content="origin-when-cross-origin" name="referrer">
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
<meta content="#474D57" name="theme-color">
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-iphone-5a9cee0e8a51212e70b90c87c12f382c428870c0ff67d1eb034d884b78d2dae7.png" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-ipad-a6eec6aeb9da138e507593b464fdac213047e49d3093fc30e90d9a995df83ba3.png" sizes="76x76" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-iphone-retina-72e2aadf86513a56e050e7f0f2355deaa19cc17ed97bbe5147847f2748e5a3e3.png" sizes="120x120" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-ipad-retina-8ebe416f5313483d9c1bc772b5bbe03ecad52a54eba443e5215a22caed2a16a2.png" sizes="152x152" />
<link color="rgb(226, 67, 41)" href="https://assets.gitlab-static.net/assets/logo-d36b5212042cebc89b96df4bf6ac24e43db316143e89926c0db839ff694d2de4.svg" rel="mask-icon">
<meta content="https://assets.gitlab-static.net/assets/msapplication-tile-1196ec67452f618d39cdd85e2e3a542f76574c071051ae7effbfde01710eb17d.png" name="msapplication-TileImage">
<meta content="#30353E" name="msapplication-TileColor">



<script nonce="zhAEJVHVkI6oCvToBLFaDg==">
//<![CDATA[
;(function(p,l,o,w,i,n,g){if(!p[i]){p.GlobalSnowplowNamespace=p.GlobalSnowplowNamespace||[];
p.GlobalSnowplowNamespace.push(i);p[i]=function(){(p[i].q=p[i].q||[]).push(arguments)
};p[i].q=p[i].q||[];n=l.createElement(o);g=l.getElementsByTagName(o)[0];n.async=1;
n.src=w;g.parentNode.insertBefore(n,g)}}(window,document,"script","https://assets.gitlab-static.net/assets/snowplow/sp-e10fd598642f1a4dd3e9e0e026f6a1ffa3c31b8a40efd92db3f92d32873baed6.js","snowplow"));

window.snowplowOptions = {"namespace":"gl","hostname":"snowplow.trx.gitlab.net","cookieDomain":".gitlab.com","appId":"gitlab","formTracking":true,"linkClickTracking":true,"igluRegistryUrl":null}


//]]>
</script>
</head>

<body class="ui-indigo tab-width-8  gl-browser-generic gl-platform-other" data-group="" data-page="help:show">

<script nonce="zhAEJVHVkI6oCvToBLFaDg==">
//<![CDATA[
gl = window.gl || {};
gl.client = {"isGeneric":true,"isOther":true};


//]]>
</script>


<header class="navbar navbar-gitlab navbar-expand-sm js-navbar" data-qa-selector="navbar">
<a class="sr-only gl-accessibility" href="#content-body" tabindex="1">Skip to content</a>
<div class="container-fluid">
<div class="header-content">
<div class="title-container">
<h1 class="title">
<a title="Dashboard" id="logo" href="/"><svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
  <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"/>
  <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"/>
  <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"/>
  <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"/>
  <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"/>
  <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"/>
  <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"/>
</svg>

<span class="logo-text d-none d-lg-block prepend-left-8">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169"><path d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3"/></svg>

</span>
</a></h1>
<ul class="list-unstyled navbar-sub-nav">
<li class="home"><a title="Projects" class="dashboard-shortcuts-projects" href="/explore">Projects
</a></li><li class=""><a title="Groups" class="dashboard-shortcuts-groups" href="/explore/groups">Groups
</a></li><li class=""><a title="Snippets" class="dashboard-shortcuts-snippets" href="/explore/snippets">Snippets
</a></li><li>
<a title="About GitLab CE" href="/help">Help</a>
</li>
</ul>

</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="nav-item d-none d-lg-block m-auto">
<div class="search search-form" data-track-event="activate_form_input" data-track-label="navbar_search" data-track-value="">
<form class="form-inline" action="/search" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><div class="search-input-container">
<div class="search-input-wrap">
<div class="dropdown" data-url="/search/autocomplete">
<input type="search" name="search" id="search" placeholder="Search or jump to…" class="search-input dropdown-menu-toggle no-outline js-search-dashboard-options" spellcheck="false" tabindex="1" autocomplete="off" data-issues-path="/dashboard/issues" data-mr-path="/dashboard/merge_requests" data-qa-selector="search_term_field" aria-label="Search or jump to…" />
<button class="hidden js-dropdown-search-toggle" data-toggle="dropdown" type="button"></button>
<div class="dropdown-menu dropdown-select js-dashboard-search-options">
<div class="dropdown-content"><ul>
<li class="dropdown-menu-empty-item">
<a>
Loading...
</a>
</li>
</ul>
</div><div class="dropdown-loading"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
</div>
<svg class="s16 search-icon"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#search"></use></svg>
<svg class="s16 clear-icon js-clear-input"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#close"></use></svg>
</div>
</div>
</div>
<input type="hidden" name="group_id" id="group_id" class="js-search-group-options" />
<input type="hidden" name="project_id" id="search_project_id" value="" class="js-search-project-options" />
<input type="hidden" name="repository_ref" id="repository_ref" />
<input type="hidden" name="nav_source" id="nav_source" value="navbar" />
<div class="search-autocomplete-opts hide" data-autocomplete-path="/search/autocomplete"></div>
</form></div>

</li>
<li class="nav-item d-inline-block d-lg-none">
<a title="Search" aria-label="Search" data-toggle="tooltip" data-placement="bottom" data-container="body" href="/search"><svg class="s16"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#search"></use></svg>
</a></li>
<li class="nav-item header-help dropdown d-none d-md-block">
<a class="header-help-dropdown-toggle" data-toggle="dropdown" href="/help"><svg class="s16"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#question"></use></svg>
<svg class="caret-down"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#angle-down"></use></svg>
</a><div class="dropdown-menu dropdown-menu-right">
<ul>
<li>
<a href="/help">Help</a>
</li>
<li>
<a href="https://about.gitlab.com/getting-help/">Support</a>
</li>
<li>
<button class="js-shortcuts-modal-trigger" type="button">
Keyboard shortcuts
<span aria-hidden class="text-secondary float-right">?</span>
</button>
</li>

<li class="divider"></li>
<li>
<a href="https://about.gitlab.com/submit-feedback">Submit feedback</a>
</li>
<li>
<a target="_blank" class="text-nowrap" href="https://about.gitlab.com/contributing">Contribute to GitLab
</a>

</li>

<li>
<a href="https://next.gitlab.com/">Switch to GitLab Next</a>
</li>
</ul>

</div>
</li>
<li class="nav-item">
<div>
<a class="btn btn-sign-in" href="/users/sign_in?redirect_to_referer=yes">Sign in / Register</a>
</div>
</li>
</ul>
</div>
<button class="navbar-toggler d-block d-sm-none" type="button">
<span class="sr-only">Toggle navigation</span>
<svg class="s12 more-icon js-navbar-toggle-right"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#ellipsis_h"></use></svg>
<svg class="s12 close-icon js-navbar-toggle-left"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#close"></use></svg>
</button>
</div>
</div>
</header>

<div class="layout-page">
<div class="content-wrapper">
<div class="mobile-overlay"></div>
<div class="alert-wrapper">









<nav class="breadcrumbs container-fluid container-limited limit-container-width" role="navigation">
<div class="breadcrumbs-container">
<div class="breadcrumbs-links js-title-container" data-qa-selector="breadcrumb_links_content">
<ul class="list-unstyled breadcrumbs-list js-breadcrumbs-list">
<li><a href="/help">Help</a><svg class="s8 breadcrumbs-list-angle"><use xlink:href="https://gitlab.com/assets/icons-71bf7df1ad95896025a943ae9f38c4e046e29fbbff1f14df5ae17d4a255feffa.svg#angle-right"></use></svg></li>

<li>
<h2 class="breadcrumbs-sub-title"><a href="/help/user/analytics/value_stream_analytics.md">Help</a></h2>
</li>
</ul>
</div>

</div>
</nav>

<div class="d-flex"></div>
</div>
<div class="container-fluid container-limited limit-container-width">
<div class="content" id="content-body">
<div class="flash-container flash-container-page sticky">
</div>

<div class="documentation md prepend-top-default">
<h1 data-sourcepos="1:1-1:24" dir="auto">&#x000A;<a id="user-content-value-stream-analytics" class="anchor" href="#value-stream-analytics" aria-hidden="true"></a>Value Stream Analytics</h1>&#x000A;<blockquote data-sourcepos="3:1-5:137" dir="auto">&#x000A;<ul data-sourcepos="3:3-5:137">&#x000A;<li data-sourcepos="3:3-3:76">Introduced as Cycle Analytics prior to GitLab 12.3 at the project level.</li>&#x000A;<li data-sourcepos="4:3-4:147">&#x000A;<a href="https://gitlab.com/gitlab-org/gitlab/issues/12077">Introduced</a> in <a href="https://about.gitlab.com/pricing/" rel="nofollow noreferrer noopener" target="_blank">GitLab Premium</a> 12.3 at the group level.</li>&#x000A;<li data-sourcepos="5:3-5:137">&#x000A;<a href="https://gitlab.com/gitlab-org/gitlab/-/merge_requests/23427">Renamed</a> from Cycle Analytics to Value Stream Analytics in GitLab 12.8.</li>&#x000A;</ul>&#x000A;</blockquote>&#x000A;<p data-sourcepos="7:1-10:43" dir="auto">Value Stream Analytics measures the time spent to go from an&#x000A;<a href="https://about.gitlab.com/blog/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/#from-idea-to-production-with-gitlab" rel="nofollow noreferrer noopener" target="_blank">idea to production</a>&#x000A;(also known as cycle time) for each of your projects. Value Stream Analytics displays the median time&#x000A;spent in each stage defined in the process.</p>&#x000A;<p data-sourcepos="12:1-12:164" dir="auto">For information on how to contribute to the development of Value Stream Analytics, see our <a href="../../development/value_stream_analytics.md">contributor documentation</a>.</p>&#x000A;<p data-sourcepos="14:1-15:68" dir="auto">NOTE: <strong>Note:</strong>&#x000A;Use the <code>cycle_analytics</code> feature flag to enable at the group level.</p>&#x000A;<p data-sourcepos="17:1-19:100" dir="auto">Value Stream Analytics is useful in order to quickly determine the velocity of a given&#x000A;project. It points to bottlenecks in the development process, enabling management&#x000A;to uncover, triage, and identify the root cause of slowdowns in the software development life cycle.</p>&#x000A;<p data-sourcepos="21:1-22:44" dir="auto">Value Stream Analytics is tightly coupled with the <a href="../../topics/gitlab_flow.md">GitLab flow</a> and&#x000A;calculates a separate median for each stage.</p>&#x000A;<h2 data-sourcepos="24:1-24:11" dir="auto">&#x000A;<a id="user-content-overview" class="anchor" href="#overview" aria-hidden="true"></a>Overview</h2>&#x000A;<p data-sourcepos="26:1-26:36" dir="auto">Value Stream Analytics is available:</p>&#x000A;<ul data-sourcepos="28:1-30:0" dir="auto">&#x000A;<li data-sourcepos="28:1-28:94">From GitLab 12.9, at the group level via <strong>Group &gt; Analytics &gt; Value Stream</strong>. <strong>(PREMIUM)</strong>&#x000A;</li>&#x000A;<li data-sourcepos="29:1-30:0">At the project level via <strong>Project &gt; Value Stream Analytics</strong>.</li>&#x000A;</ul>&#x000A;<p data-sourcepos="31:1-31:91" dir="auto">There are seven stages that are tracked as part of the Value Stream Analytics calculations.</p>&#x000A;<ul data-sourcepos="33:1-47:0" dir="auto">&#x000A;<li data-sourcepos="33:1-34:78">&#x000A;<strong>Issue</strong> (Tracker)&#x000A;<ul data-sourcepos="34:3-34:78">&#x000A;<li data-sourcepos="34:3-34:78">Time to schedule an issue (by milestone or by adding it to an issue board)</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="35:1-36:24">&#x000A;<strong>Plan</strong> (Board)&#x000A;<ul data-sourcepos="36:3-36:24">&#x000A;<li data-sourcepos="36:3-36:24">Time to first commit</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="37:1-38:34">&#x000A;<strong>Code</strong> (IDE)&#x000A;<ul data-sourcepos="38:3-38:34">&#x000A;<li data-sourcepos="38:3-38:34">Time to create a merge request</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="39:1-40:48">&#x000A;<strong>Test</strong> (CI)&#x000A;<ul data-sourcepos="40:3-40:48">&#x000A;<li data-sourcepos="40:3-40:48">Time it takes GitLab CI/CD to test your code</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="41:1-42:29">&#x000A;<strong>Review</strong> (Merge Request/MR)&#x000A;<ul data-sourcepos="42:3-42:29">&#x000A;<li data-sourcepos="42:3-42:29">Time spent on code review</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="43:1-44:52">&#x000A;<strong>Staging</strong> (Continuous Deployment)&#x000A;<ul data-sourcepos="44:3-44:52">&#x000A;<li data-sourcepos="44:3-44:52">Time between merging and deploying to production</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="45:1-47:0">&#x000A;<strong>Total</strong> (Total)&#x000A;<ul data-sourcepos="46:3-47:0">&#x000A;<li data-sourcepos="46:3-47:0">Total lifecycle time. That is, the velocity of the project or team. <a href="https://gitlab.com/gitlab-org/gitlab/issues/38317">Previously known</a> as <strong>Production</strong>.</li>&#x000A;</ul>&#x000A;</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="48:1-48:14" dir="auto">&#x000A;<a id="user-content-date-ranges" class="anchor" href="#date-ranges" aria-hidden="true"></a>Date ranges</h2>&#x000A;<blockquote data-sourcepos="50:1-50:81" dir="auto">&#x000A;<p data-sourcepos="50:3-50:81"><a href="https://gitlab.com/gitlab-org/gitlab/issues/13216">Introduced</a> in GitLab 12.4.</p>&#x000A;</blockquote>&#x000A;<p data-sourcepos="52:1-52:89" dir="auto">GitLab provides the ability to filter analytics based on a date range. To filter results:</p>&#x000A;<ol data-sourcepos="54:1-57:0" dir="auto">&#x000A;<li data-sourcepos="54:1-54:18">Select a group.</li>&#x000A;<li data-sourcepos="55:1-55:31">Optionally select a project.</li>&#x000A;<li data-sourcepos="56:1-57:0">Select a date range using the available date pickers.</li>&#x000A;</ol>&#x000A;<h2 data-sourcepos="58:1-58:27" dir="auto">&#x000A;<a id="user-content-how-the-data-is-measured" class="anchor" href="#how-the-data-is-measured" aria-hidden="true"></a>How the data is measured</h2>&#x000A;<p data-sourcepos="60:1-62:24" dir="auto">Value Stream Analytics records cycle time and data based on the project issues with the&#x000A;exception of the staging and total stages, where only data deployed to&#x000A;production are measured.</p>&#x000A;<p data-sourcepos="64:1-66:20" dir="auto">Specifically, if your CI is not set up and you have not defined a <code>production</code>&#x000A;or <code>production/*</code> <a href="../../ci/yaml/README.md#environment">environment</a>, then you will not have any&#x000A;data for this stage.</p>&#x000A;<p data-sourcepos="68:1-68:77" dir="auto">Each stage of Value Stream Analytics is further described in the table below.</p>&#x000A;<table data-sourcepos="70:1-78:220" dir="auto">&#x000A;<thead>&#x000A;<tr data-sourcepos="70:1-70:31">&#x000A;<th data-sourcepos="70:2-70:12"><strong>Stage</strong></th>&#x000A;<th data-sourcepos="70:14-70:30"><strong>Description</strong></th>&#x000A;</tr>&#x000A;</thead>&#x000A;<tbody>&#x000A;<tr data-sourcepos="72:1-72:304">&#x000A;<td data-sourcepos="72:2-72:12">Issue</td>&#x000A;<td data-sourcepos="72:14-72:303">Measures the median time between creating an issue and taking action to solve it, by either labeling it or adding it to a milestone, whatever comes first. The label will be tracked only if it already has an <a href="../project/issue_board.md#creating-a-new-list">Issue Board list</a> created for it.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="73:1-73:476">&#x000A;<td data-sourcepos="73:2-73:12">Plan</td>&#x000A;<td data-sourcepos="73:14-73:475">Measures the median time between the action you took for the previous stage, and pushing the first commit to the branch. The very first commit of the branch is the one that triggers the separation between <strong>Plan</strong> and <strong>Code</strong>, and at least one of the commits in the branch needs to contain the related issue number (e.g., <code>#42</code>). If none of the commits in the branch mention the related issue number, it is not considered to the measurement time of the stage.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="74:1-74:572">&#x000A;<td data-sourcepos="74:2-74:12">Code</td>&#x000A;<td data-sourcepos="74:14-74:571">Measures the median time between pushing a first commit (previous stage) and creating a merge request (MR) related to that commit. The key to keep the process tracked is to include the <a href="../project/issues/managing_issues.md#closing-issues-automatically">issue closing pattern</a> to the description of the merge request (for example, <code>Closes #xxx</code>, where <code>xxx</code> is the number of the issue related to this merge request). If the issue closing pattern is not present in the merge request description, the MR is not considered to the measurement time of the stage.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="75:1-75:280">&#x000A;<td data-sourcepos="75:2-75:12">Test</td>&#x000A;<td data-sourcepos="75:14-75:279">Measures the median time to run the entire pipeline for that project. It's related to the time GitLab CI/CD takes to run every job for the commits pushed to that merge request defined in the previous stage. It is basically the start-&gt;finish time for all pipelines.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="76:1-76:152">&#x000A;<td data-sourcepos="76:2-76:12">Review</td>&#x000A;<td data-sourcepos="76:14-76:151">Measures the median time taken to review the merge request that has a closing issue pattern, between its creation and until it's merged.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="77:1-77:369">&#x000A;<td data-sourcepos="77:2-77:12">Staging</td>&#x000A;<td data-sourcepos="77:14-77:368">Measures the median time between merging the merge request with a closing issue pattern until the very first deployment to production. It's tracked by the environment set to <code>production</code> or matching <code>production/*</code> (case-sensitive, <code>Production</code> won't work) in your GitLab CI/CD configuration. If there isn't a production environment, this is not tracked.</td>&#x000A;</tr>&#x000A;<tr data-sourcepos="78:1-78:220">&#x000A;<td data-sourcepos="78:2-78:8">Total</td>&#x000A;<td data-sourcepos="78:10-78:219">The sum of all time (medians) taken to run the entire process, from issue creation to deploying the code to production. <a href="https://gitlab.com/gitlab-org/gitlab/issues/38317">Previously known</a> as <strong>Production</strong>.</td>&#x000A;</tr>&#x000A;</tbody>&#x000A;</table>&#x000A;<p data-sourcepos="80:1-80:34" dir="auto">How this works, behind the scenes:</p>&#x000A;<ol data-sourcepos="82:1-91:0" dir="auto">&#x000A;<li data-sourcepos="82:1-85:14">Issues and merge requests are grouped together in pairs, such that for each&#x000A;<code>&lt;issue, merge request&gt;</code> pair, the merge request has the <a href="../project/issues/managing_issues.md#closing-issues-automatically">issue closing pattern</a>&#x000A;for the corresponding issue. All other issues and merge requests are <strong>not</strong>&#x000A;considered.</li>&#x000A;<li data-sourcepos="86:1-87:86">Then the <code>&lt;issue, merge request&gt;</code> pairs are filtered out by last XX days (specified&#x000A;by the UI - default is 90 days). So it prohibits these pairs from being considered.</li>&#x000A;<li data-sourcepos="88:1-91:0">For the remaining <code>&lt;issue, merge request&gt;</code> pairs, we check the information that&#x000A;we need for the stages, like issue creation date, merge request merge time,&#x000A;etc.</li>&#x000A;</ol>&#x000A;<p data-sourcepos="92:1-93:63" dir="auto">To sum up, anything that doesn't follow <a href="../../workflow/gitlab_flow.md">GitLab flow</a> will not be tracked and the&#x000A;Value Stream Analytics dashboard will not present any data for:</p>&#x000A;<ul data-sourcepos="95:1-99:0" dir="auto">&#x000A;<li data-sourcepos="95:1-95:44">Merge requests that do not close an issue.</li>&#x000A;<li data-sourcepos="96:1-96:100">Issues not labeled with a label present in the Issue Board or for issues not assigned a milestone.</li>&#x000A;<li data-sourcepos="97:1-99:0">Staging and production stages, if the project has no <code>production</code> or <code>production/*</code>&#x000A;environment.</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="100:1-100:19" dir="auto">&#x000A;<a id="user-content-example-workflow" class="anchor" href="#example-workflow" aria-hidden="true"></a>Example workflow</h2>&#x000A;<p data-sourcepos="102:1-106:27" dir="auto">Below is a simple fictional workflow of a single cycle that happens in a&#x000A;single day passing through all seven stages. Note that if a stage does not have&#x000A;a start and a stop mark, it is not measured and hence not calculated in the median&#x000A;time. It is assumed that milestones are created and CI for testing and setting&#x000A;environments is configured.</p>&#x000A;<ol data-sourcepos="108:1-127:0" dir="auto">&#x000A;<li data-sourcepos="108:1-108:56">Issue is created at 09:00 (start of <strong>Issue</strong> stage).</li>&#x000A;<li data-sourcepos="109:1-110:19">Issue is added to a milestone at 11:00 (stop of <strong>Issue</strong> stage / start of&#x000A;<strong>Plan</strong> stage).</li>&#x000A;<li data-sourcepos="111:1-112:9">Start working on the issue, create a branch locally and make one commit at&#x000A;12:00.</li>&#x000A;<li data-sourcepos="113:1-114:54">Make a second commit to the branch which mentions the issue number at 12.30&#x000A;(stop of <strong>Plan</strong> stage / start of <strong>Code</strong> stage).</li>&#x000A;<li data-sourcepos="115:1-117:22">Push branch and create a merge request that contains the <a href="../project/issues/managing_issues.md#closing-issues-automatically">issue closing pattern</a>&#x000A;in its description at 14:00 (stop of <strong>Code</strong> stage / start of <strong>Test</strong> and&#x000A;<strong>Review</strong> stages).</li>&#x000A;<li data-sourcepos="118:1-119:39">The CI starts running your scripts defined in <a href="../../ci/yaml/README.md"><code>.gitlab-ci.yml</code></a> and&#x000A;takes 5min (stop of <strong>Test</strong> stage).</li>&#x000A;<li data-sourcepos="120:1-121:77">Review merge request, ensure that everything is OK and merge the merge&#x000A;request at 19:00. (stop of <strong>Review</strong> stage / start of <strong>Staging</strong> stage).</li>&#x000A;<li data-sourcepos="122:1-123:72">Now that the merge request is merged, a deployment to the <code>production</code>&#x000A;environment starts and finishes at 19:30 (stop of <strong>Staging</strong> stage).</li>&#x000A;<li data-sourcepos="124:1-127:0">The cycle completes and the sum of the median times of the previous stages&#x000A;is recorded to the <strong>Total</strong> stage. That is the time between creating an&#x000A;issue and deploying its relevant merge request to production.</li>&#x000A;</ol>&#x000A;<p data-sourcepos="128:1-129:28" dir="auto">From the above example you can conclude the time it took each stage to complete&#x000A;as long as their total time:</p>&#x000A;<ul data-sourcepos="131:1-141:0" dir="auto">&#x000A;<li data-sourcepos="131:1-131:31">&#x000A;<strong>Issue</strong>: 2h (11:00 - 09:00)</li>&#x000A;<li data-sourcepos="132:1-132:30">&#x000A;<strong>Plan</strong>: 1h (12:00 - 11:00)</li>&#x000A;<li data-sourcepos="133:1-133:30">&#x000A;<strong>Code</strong>: 2h (14:00 - 12:00)</li>&#x000A;<li data-sourcepos="134:1-134:16">&#x000A;<strong>Test</strong>: 5min</li>&#x000A;<li data-sourcepos="135:1-135:32">&#x000A;<strong>Review</strong>: 5h (19:00 - 14:00)</li>&#x000A;<li data-sourcepos="136:1-136:36">&#x000A;<strong>Staging</strong>: 30min (19:30 - 19:00)</li>&#x000A;<li data-sourcepos="137:1-141:0">&#x000A;<strong>Total</strong>: Since this stage measures the sum of median time of all&#x000A;previous stages, we cannot calculate it if we don't know the status of the&#x000A;stages before. In case this is the very first cycle that is run in the project,&#x000A;then the <strong>Total</strong> time is 10h 30min (19:30 - 09:00)</li>&#x000A;</ul>&#x000A;<p data-sourcepos="142:1-142:12" dir="auto">A few notes:</p>&#x000A;<ul data-sourcepos="144:1-153:0" dir="auto">&#x000A;<li data-sourcepos="144:1-146:35">In the above example we demonstrated that it doesn't matter if your first&#x000A;commit doesn't mention the issue number, you can do this later in any commit&#x000A;of the branch you are working on.</li>&#x000A;<li data-sourcepos="147:1-149:10">You can see that the <strong>Test</strong> stage is not calculated to the overall time of&#x000A;the cycle since it is included in the <strong>Review</strong> process (every MR should be&#x000A;tested).</li>&#x000A;<li data-sourcepos="150:1-153:0">The example above was just <strong>one cycle</strong> of the seven stages. Add multiple&#x000A;cycles, calculate their median time and the result is what the dashboard of&#x000A;Value Stream Analytics is showing.</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="154:1-154:38" dir="auto">&#x000A;<a id="user-content-customizable-value-stream-analytics" class="anchor" href="#customizable-value-stream-analytics" aria-hidden="true"></a>Customizable Value Stream Analytics</h2>&#x000A;<blockquote data-sourcepos="156:1-156:83" dir="auto">&#x000A;<p data-sourcepos="156:3-156:83"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/12196">Introduced</a> in GitLab 12.9.</p>&#x000A;</blockquote>&#x000A;<p data-sourcepos="158:1-160:42" dir="auto">The default stages are designed to work straight out of the box, but they might not be suitable for&#x000A;all teams. Different teams use different approaches to building software, so some teams might want&#x000A;to customize their Value Stream Analytics.</p>&#x000A;<p data-sourcepos="162:1-163:21" dir="auto">GitLab allows users to hide default stages and create custom stages that align better to their&#x000A;development workflow.</p>&#x000A;<p data-sourcepos="165:1-166:143" dir="auto">NOTE: <strong>Note:</strong>&#x000A;Customizability is <a href="https://gitlab.com/gitlab-org/gitlab/-/issues/35823#note_272558950">only available for group-level</a> Value Stream Analytics.</p>&#x000A;<h3 data-sourcepos="168:1-168:18" dir="auto">&#x000A;<a id="user-content-adding-a-stage" class="anchor" href="#adding-a-stage" aria-hidden="true"></a>Adding a stage</h3>&#x000A;<p data-sourcepos="170:1-171:27" dir="auto">In the following example we're creating a new stage that measures and tracks issues from creation&#x000A;time until they are closed.</p>&#x000A;<ol data-sourcepos="173:1-180:0" dir="auto">&#x000A;<li data-sourcepos="173:1-173:57">Navigate to your group's <strong>Analytics &gt; Value Stream</strong>.</li>&#x000A;<li data-sourcepos="174:1-174:36">Click the <strong>Add a stage</strong> button.</li>&#x000A;<li data-sourcepos="175:1-178:29">Fill in the new stage form:&#x000A;<ul data-sourcepos="176:4-178:29">&#x000A;<li data-sourcepos="176:4-176:33">Name: Issue start to finish.</li>&#x000A;<li data-sourcepos="177:4-177:32">Start event: Issue created.</li>&#x000A;<li data-sourcepos="178:4-178:29">End event: Issue closed.</li>&#x000A;</ul>&#x000A;</li>&#x000A;<li data-sourcepos="179:1-180:0">Click the <strong>Add stage</strong> button.</li>&#x000A;</ol>&#x000A;<p data-sourcepos="181:1-181:96" dir="auto"><a class="no-attachment-icon" href="img/new_vsm_stage_v12_9.png" target="_blank" rel="noopener noreferrer"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="New Value Stream Analytics Stage" title="Form for creating a new stage" class="lazy" data-src="img/new_vsm_stage_v12_9.png"></a></p>&#x000A;<p data-sourcepos="183:1-184:6" dir="auto">The new stage is persisted and it will always show up on the Value Stream Analytics page for your&#x000A;group.</p>&#x000A;<p data-sourcepos="186:1-186:90" dir="auto">If you want to alter or delete the stage, you can easily do that for customized stages by:</p>&#x000A;<ol data-sourcepos="188:1-190:0" dir="auto">&#x000A;<li data-sourcepos="188:1-188:27">Hovering over the stage.</li>&#x000A;<li data-sourcepos="189:1-190:0">Clicking the vertical ellipsis (<strong>{ellipsis_v}</strong>) button that appears.</li>&#x000A;</ol>&#x000A;<p data-sourcepos="191:1-191:62" dir="auto"><a class="no-attachment-icon" href="img/vsm_stage_list_v12_9.png" target="_blank" rel="noopener noreferrer"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Value Stream Analytics Stages" class="lazy" data-src="img/vsm_stage_list_v12_9.png"></a></p>&#x000A;<p data-sourcepos="193:1-193:55" dir="auto">Creating a custom stage requires specifying two events:</p>&#x000A;<ul data-sourcepos="195:1-197:0" dir="auto">&#x000A;<li data-sourcepos="195:1-195:10">A start.</li>&#x000A;<li data-sourcepos="196:1-197:0">An end.</li>&#x000A;</ul>&#x000A;<p data-sourcepos="198:1-199:11" dir="auto">Be careful to choose a start event that occurs <em>before</em> your end event. For example, consider a&#x000A;stage that:</p>&#x000A;<ul data-sourcepos="201:1-203:0" dir="auto">&#x000A;<li data-sourcepos="201:1-201:44">Started when an issue is added to a board.</li>&#x000A;<li data-sourcepos="202:1-203:0">Ended when the issue is created.</li>&#x000A;</ul>&#x000A;<p data-sourcepos="204:1-206:78" dir="auto">This stage would not work because the end event has already happened when the start event occurs.&#x000A;To prevent such invalid stages, the UI prohibits incompatible start and end events. After you select&#x000A;the start event, the stop event dropdown will only list the compatible events.</p>&#x000A;<h3 data-sourcepos="208:1-208:22" dir="auto">&#x000A;<a id="user-content-re-ordering-stages" class="anchor" href="#re-ordering-stages" aria-hidden="true"></a>Re-ordering stages</h3>&#x000A;<blockquote data-sourcepos="210:1-210:85" dir="auto">&#x000A;<p data-sourcepos="210:3-210:85"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/196698">Introduced</a> in GitLab 12.10.</p>&#x000A;</blockquote>&#x000A;<p data-sourcepos="212:1-212:145" dir="auto">Once a custom stage has been added, you can "drag and drop" stages to rearrange their order. These changes are automatically saved to the system.</p>&#x000A;<h3 data-sourcepos="214:1-214:22" dir="auto">&#x000A;<a id="user-content-label-based-stages" class="anchor" href="#label-based-stages" aria-hidden="true"></a>Label based stages</h3>&#x000A;<p data-sourcepos="216:1-216:103" dir="auto">The pre-defined start and end events can cover many use cases involving both issues and merge requests.</p>&#x000A;<p data-sourcepos="218:1-220:33" dir="auto">For supporting more complex workflows, use stages based on group labels. These events are based on&#x000A;labels being added or removed. In particular, <a href="../project/labels.md#scoped-labels-premium">scoped labels</a>&#x000A;are useful for complex workflows.</p>&#x000A;<p data-sourcepos="222:1-222:101" dir="auto">In this example, we'd like to measure more accurate code review times. The workflow is the following:</p>&#x000A;<ul data-sourcepos="224:1-226:0" dir="auto">&#x000A;<li data-sourcepos="224:1-224:106">When the code review starts, the reviewer adds <code>workflow::code_review_start</code> label to the merge request.</li>&#x000A;<li data-sourcepos="225:1-226:0">When the code review is finished, the reviewer adds <code>workflow::code_review_complete</code> label to the merge request.</li>&#x000A;</ul>&#x000A;<p data-sourcepos="227:1-227:42" dir="auto">Creating a new stage called "Code Review":</p>&#x000A;<p data-sourcepos="229:1-229:138" dir="auto"><a class="no-attachment-icon" href="img/label_based_stage_vsm_v12_9.png" target="_blank" rel="noopener noreferrer"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="New Label Based Value Stream Analytics Stage" title="Creating a label based Value Stream Analytics Stage" class="lazy" data-src="img/label_based_stage_vsm_v12_9.png"></a></p>&#x000A;<h3 data-sourcepos="231:1-231:24" dir="auto">&#x000A;<a id="user-content-hiding-unused-stages" class="anchor" href="#hiding-unused-stages" aria-hidden="true"></a>Hiding unused stages</h3>&#x000A;<p data-sourcepos="233:1-234:53" dir="auto">Sometimes certain default stages are not relevant to a team. In this case, you can easily hide stages&#x000A;so they no longer appear in the list. To hide stages:</p>&#x000A;<ol data-sourcepos="236:1-239:0" dir="auto">&#x000A;<li data-sourcepos="236:1-236:50">Add a custom stage to activate customizability.</li>&#x000A;<li data-sourcepos="237:1-237:49">Hover over the default stage you want to hide.</li>&#x000A;<li data-sourcepos="238:1-239:0">Click the vertical ellipsis (<strong>{ellipsis_v}</strong>) button that appears and select <strong>Hide stage</strong>.</li>&#x000A;</ol>&#x000A;<p data-sourcepos="240:1-240:54" dir="auto">To recover a default stage that was previously hidden:</p>&#x000A;<ol data-sourcepos="242:1-245:0" dir="auto">&#x000A;<li data-sourcepos="242:1-242:32">Click <strong>Add a stage</strong> button.</li>&#x000A;<li data-sourcepos="243:1-243:70">In the top right corner open the <strong>Recover hidden stage</strong> dropdown.</li>&#x000A;<li data-sourcepos="244:1-245:0">Select a stage.</li>&#x000A;</ol>&#x000A;<h2 data-sourcepos="246:1-246:27" dir="auto">&#x000A;<a id="user-content-days-to-completion-chart" class="anchor" href="#days-to-completion-chart" aria-hidden="true"></a>Days to completion chart</h2>&#x000A;<blockquote data-sourcepos="248:1-248:91" dir="auto">&#x000A;<p data-sourcepos="248:3-248:91"><a href="https://gitlab.com/gitlab-org/gitlab/-/merge_requests/21631">Introduced</a> in GitLab 12.6.</p>&#x000A;</blockquote>&#x000A;<p data-sourcepos="250:1-250:89" dir="auto">This chart visually depicts the total number of days it takes for cycles to be completed.</p>&#x000A;<p data-sourcepos="252:1-254:29" dir="auto">This chart uses the global page filters for displaying data based on the selected&#x000A;group, projects, and timeframe. In addition, specific stages can be selected&#x000A;from within the chart itself.</p>&#x000A;<h3 data-sourcepos="256:1-256:21" dir="auto">&#x000A;<a id="user-content-chart-median-line" class="anchor" href="#chart-median-line" aria-hidden="true"></a>Chart median line</h3>&#x000A;<blockquote data-sourcepos="258:1-258:81" dir="auto">&#x000A;<p data-sourcepos="258:3-258:81"><a href="https://gitlab.com/gitlab-org/gitlab/issues/36675">Introduced</a> in GitLab 12.7.</p>&#x000A;</blockquote>&#x000A;<p data-sourcepos="260:1-263:31" dir="auto">The median line on the chart displays data that is offset by the number of days selected.&#x000A;For example, if 30 days worth of data has been selected (for example, 2019-12-16 to 2020-01-15) the&#x000A;median line will represent the previous 30 days worth of data (2019-11-16 to 2019-12-16)&#x000A;as a metric to compare against.</p>&#x000A;<h3 data-sourcepos="265:1-265:19" dir="auto">&#x000A;<a id="user-content-disabling-chart" class="anchor" href="#disabling-chart" aria-hidden="true"></a>Disabling chart</h3>&#x000A;<p data-sourcepos="267:1-268:81" dir="auto">This chart is enabled by default. If you have a self-managed instance, an&#x000A;administrator can open a Rails console and disable it with the following command:</p>&#x000A;<pre class="code highlight js-syntax-highlight ruby" lang="ruby" v-pre="true"><code><span id="LC1" class="line" lang="ruby"><span class="no">Feature</span><span class="p">.</span><span class="nf">disable</span><span class="p">(</span><span class="ss">:cycle_analytics_scatterplot_enabled</span><span class="p">)</span></span></code></pre>&#x000A;<h3 data-sourcepos="274:1-274:31" dir="auto">&#x000A;<a id="user-content-disabling-chart-median-line" class="anchor" href="#disabling-chart-median-line" aria-hidden="true"></a>Disabling chart median line</h3>&#x000A;<p data-sourcepos="276:1-277:81" dir="auto">This chart's median line is enabled by default. If you have a self-managed instance, an&#x000A;administrator can open a Rails console and disable it with the following command:</p>&#x000A;<pre class="code highlight js-syntax-highlight ruby" lang="ruby" v-pre="true"><code><span id="LC1" class="line" lang="ruby"><span class="no">Feature</span><span class="p">.</span><span class="nf">disable</span><span class="p">(</span><span class="ss">:cycle_analytics_scatterplot_median_enabled</span><span class="p">)</span></span></code></pre>&#x000A;<h2 data-sourcepos="283:1-283:14" dir="auto">&#x000A;<a id="user-content-permissions" class="anchor" href="#permissions" aria-hidden="true"></a>Permissions</h2>&#x000A;<p data-sourcepos="285:1-285:76" dir="auto">The current permissions on the Project Value Stream Analytics dashboard are:</p>&#x000A;<ul data-sourcepos="287:1-290:0" dir="auto">&#x000A;<li data-sourcepos="287:1-287:38">Public projects - anyone can access.</li>&#x000A;<li data-sourcepos="288:1-288:56">Internal projects - any authenticated user can access.</li>&#x000A;<li data-sourcepos="289:1-290:0">Private projects - any member Guest and above can access.</li>&#x000A;</ul>&#x000A;<p data-sourcepos="291:1-291:74" dir="auto">You can <a href="../../ci/yaml/README.md">read more about permissions</a> in general.</p>&#x000A;<p data-sourcepos="293:1-297:39" dir="auto">NOTE: <strong>Note:</strong>&#x000A;As of GitLab 12.3, the project-level page is deprecated. You should access&#x000A;project-level Value Stream Analytics from <strong>Analytics &gt; Value Stream Analytics</strong> in the top&#x000A;navigation bar. We will ensure that the same project-level functionality is available&#x000A;to CE users in the new analytics space.</p>&#x000A;<p data-sourcepos="299:1-299:77" dir="auto">For Value Stream Analytics functionality introduced in GitLab 12.3 and later:</p>&#x000A;<ul data-sourcepos="301:1-304:0" dir="auto">&#x000A;<li data-sourcepos="301:1-301:43">Users must have Reporter access or above.</li>&#x000A;<li data-sourcepos="302:1-304:0">Features are available only on&#x000A;<a href="https://about.gitlab.com/pricing/" rel="nofollow noreferrer noopener" target="_blank">Premium or Silver tiers</a> and above.</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="305:1-305:18" dir="auto">&#x000A;<a id="user-content-troubleshooting" class="anchor" href="#troubleshooting" aria-hidden="true"></a>Troubleshooting</h2>&#x000A;<p data-sourcepos="307:1-307:77" dir="auto">If you see an error as listed in the following table, try the noted solution:</p>&#x000A;<table data-sourcepos="309:1-311:274" dir="auto">&#x000A;<thead>&#x000A;<tr data-sourcepos="309:1-309:276">&#x000A;<th data-sourcepos="309:2-309:46">Error</th>&#x000A;<th data-sourcepos="309:48-309:275">Solution</th>&#x000A;</tr>&#x000A;</thead>&#x000A;<tbody>&#x000A;<tr data-sourcepos="311:1-311:274">&#x000A;<td data-sourcepos="311:2-311:46">There was an error fetching the top labels.</td>&#x000A;<td data-sourcepos="311:48-311:273">Manually enable tasks by type feature in the <a href="../../administration/troubleshooting/navigating_gitlab_via_rails_console.md#starting-a-rails-console-session">rails console</a>, specifically <code>Feature.enable(:tasks_by_type_chart)</code>.</td>&#x000A;</tr>&#x000A;</tbody>&#x000A;</table>&#x000A;<h2 data-sourcepos="313:1-313:17" dir="auto">&#x000A;<a id="user-content-more-resources" class="anchor" href="#more-resources" aria-hidden="true"></a>More resources</h2>&#x000A;<p data-sourcepos="315:1-315:67" dir="auto">Learn more about Value Stream Analytics in the following resources:</p>&#x000A;<ul data-sourcepos="317:1-319:122" dir="auto">&#x000A;<li data-sourcepos="317:1-317:114">&#x000A;<a href="https://about.gitlab.com/stages-devops-lifecycle/value-stream-analytics/" rel="nofollow noreferrer noopener" target="_blank">Value Stream Analytics feature page</a>.</li>&#x000A;<li data-sourcepos="318:1-318:130">&#x000A;<a href="https://about.gitlab.com/blog/2016/09/16/feature-preview-introducing-cycle-analytics/" rel="nofollow noreferrer noopener" target="_blank">Value Stream Analytics feature preview</a>.</li>&#x000A;<li data-sourcepos="319:1-319:122">&#x000A;<a href="https://about.gitlab.com/blog/2016/09/21/cycle-analytics-feature-highlight/" rel="nofollow noreferrer noopener" target="_blank">Value Stream Analytics feature highlight</a>.</li>&#x000A;</ul>
</div>

</div>
</div>
</div>
</div>




</body>
</html>

